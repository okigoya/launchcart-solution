package org.launchcode.launchcart.models;

import org.junit.Before;
import org.junit.Test;

import static junit.framework.TestCase.assertTrue;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

/**
 * Created by LaunchCode
 */
public class TestCart {

    private Cart cart;

    @Before
    public void setupCart() {
        cart = new Cart();
    }

    @Test
    public void testAddItem() {
        Item item = new Item("Test Item", 5);
        assertFalse(cart.getItems().contains(item));
        cart.addItem(item);
        assertTrue(cart.getItems().contains(item));
    }

    // TODO: Add Total to Cart: testComputeTotal -- why should this test go here?
    @Test
    public void testCartTotal() {
        Item item = new Item("Test Item 1", 5.00);
        Item item2 = new Item("Test Item 2", 15.50);
        cart.addItem(item);
        cart.addItem(item2);
        assertEquals(cart.computeTotal(), 20.50, .001);
    }

    @Test
    public void testRemoveItemFromCart() {
        Item item1 = new Item("Test Item 1", 12.25);
        Item item2 = new Item("Test Item 2", 15.75);
        cart.addItem(item1);
        cart.addItem(item2);
        cart.removeItem(item1);
        assertEquals(cart.computeTotal(), 15.75, .001);
    }
}
