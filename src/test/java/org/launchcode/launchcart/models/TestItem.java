package org.launchcode.launchcart.models;


import org.junit.Test;

import static junit.framework.TestCase.assertTrue;

public class TestItem {


    @Test
    public void testNewItem() {
        Item item = new Item("Test Item 1", 5.55, true);
        assertTrue(item.isNewItem());
    }
}
