package org.launchcode.launchcart.models;


import javax.persistence.Entity;
import javax.validation.constraints.NotNull;

/**
 * Created by LaunchCode
 */
@Entity
public class Item extends AbstractEntity {

    private String name;

    private double price;

    private String description;

    @NotNull
    private boolean newItem;

    public Item() {
        super();
    }

    public Item (String name, double price, String description, boolean newItem) {
        this();
        this.name = name;
        this.price = price;
        this.description = description;
        this.newItem = newItem;
    }

    public Item(String name, double price) {
        this(name, price, "", true);
    }

    public Item(String name, double price, boolean newItem) {
        this(name, price, "", newItem);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public boolean isNewItem() {
        return newItem;
    }

    public void setNewItem(boolean newItem) {
        this.newItem = newItem;
    }
}
